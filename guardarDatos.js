//Crear una función JavaScript para guardar los datos de estudiantes
//(Cédula, apellidos, nombres, dirección, semestre, paralelo, correo electrónico) 
//PL/SQL. Validar los datos antes de guardarlos.

const conexion = require('./cone_base_datos')

module.exports = app => {
    const connect = conexion

    app.post('/registrar_estudiante', (req, res) => {
        const cedula = req.body.cedula
        const apellidos = req.body.apellidos
        const nombres = req.body.nombres
        const dirección = req.body.dirección
        const semestre = req.body.semestre
        const paralelo = req.body.paralelo
        const email = req.body.email

        connect.query(`INSERT INTO estudiantes VALUES(?, ?, ?, ?, ?, ?, ?)`, [cedula, apellidos, nombres, dirección, semestre, paralelo, email],
            (error, resultado) => {
                res.redirect('/index');
            })
    })
}